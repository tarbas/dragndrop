  card.onmousedown = function (e) {

      // получить координаты карточки и контейнера
      let coords = getCoords(card)
      let boardCoord = getCoords(board)
      // учесть смещение карточки при клике на ней
      let shiftX = e.pageX - coords.left
      let shiftY = e.pageY - coords.top
      console.log('shiftX- ', shiftX);
      
      // учесть смещение карточки при ограничении по контйнеру, для верха и лева можно брать данные выше
      let shiftBottom = e.pageY - coords.bottom
      let shiftRight = e.pageX - coords.right



      //  отследить нажатие
      // подготовить к перемещению
      // разместить на том же месте, но в абсолютных координатах

      card.style.position = 'absolute'
      moveAt(e)
      // переместим в body, чтобы мяч был точно не внутри position:relative
      board.appendChild(card)

      card.style.zIndex = 1000 // показывать мяч над другими элементами

      // передвинуть мяч под координаты курсора
      // и сдвинуть на половину ширины/высоты для центрирования
      function moveAt(e) {

          card.style.left = e.pageX - shiftX + 'px'
          card.style.top = e.pageY - shiftY + 'px'


      }

      // перемещать по экрану
      document.onmousemove = function (e) {
          // Проверить что бы не выходила карточка за контейнер
          if (e.pageY - shiftY > boardCoord.top && e.pageY - shiftBottom < boardCoord.bottom &&
              e.pageX - shiftX > boardCoord.left && e.pageX - shiftRight < boardCoord.right) {
              moveAt(e)
          }
      }
      // отследить окончание переноса
      card.onmouseup = function () {
          document.onmousemove = null
          card.onmouseup = null
      }
  }

  // метод для вычисления координтав любого элемента

  function getCoords(elem) {
      let box = elem.getBoundingClientRect()

      return {
          top: box.top + pageYOffset,
          left: box.left + pageXOffset,
          bottom: box.top + box.height + pageYOffset,
          right: box.left + box.width + pageXOffset
      }
  }
 